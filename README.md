# How to contribute:  
1. create a fork
1. Make changes to data.json  
1. create pull request against pilot  
* once approved - changes will be deployed to pilot.secdsm.org  

# Merge Access
* hit up zoomequipd in slack for merge access to pilot

# Deploy to secdsm.org
* when changes are ready for "prod" - create a pull request to merge pilot to master